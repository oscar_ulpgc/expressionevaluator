package lexical;

import java.util.ArrayList;
import parser.Token;

public class LexicalAnalyzer {
    
    public Token[] analyze(String text) {
        
        ArrayList<Token> tokens = new ArrayList();
        String digit = "";
        text = removeSpaces(text);
        
        for (char character : text.toCharArray()) {
            if(TokenBuilder.isOperator(character)){
                addConstant(digit, tokens);
                addOperator(tokens, character);
                digit = "";
                continue;
            }
            digit += character;
        }
        
        addConstant(digit, tokens);
        return toTokenArray(tokens);
    }

    private Token[] toTokenArray(ArrayList<Token> tokens) {
        Token[] result = new Token[tokens.size()];
        tokens.toArray(result);
        return result;
    }

    private String removeSpaces(String text) {
        return text.replaceAll("\\s","");
    }

    private void addConstant(String digit, ArrayList<Token> tokens) {
        if(!digit.equals("")){
            tokens.add(TokenBuilder.buildConstant(digit));
        }
    }

    private void addOperator(ArrayList<Token> tokens, char character) {
        tokens.add(TokenBuilder.buildOperator(character));
    }
}
