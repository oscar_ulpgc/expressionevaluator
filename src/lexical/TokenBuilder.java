package lexical;

import parser.Constant;
import parser.OperatorSymbolColletion;
import parser.Token;

public class TokenBuilder {

    public static Constant buildConstant(String constant){
        if(!isNumeric(constant))
            return Token.constant(constant);
        if(isDouble(constant))
            return Token.constant(Double.parseDouble(constant));
        return Token.constant(Integer.parseInt(constant));
    }
    
    static Token buildOperator(char symbol) {
        return new OperatorSymbolColletion().getToken(symbol);
    }
    
    public static boolean isOperator(char symbol){
        return new OperatorSymbolColletion().isOperator(symbol);
    }
    
    private static boolean isNumeric(String str){
      return str.matches("\\d+(\\.\\d+)?");
    }
    
    private static boolean isDouble(String str){
      return str.matches("\\d+(\\.\\d+)");
    }
}
