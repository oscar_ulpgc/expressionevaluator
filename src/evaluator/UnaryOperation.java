package evaluator;

import evaluator.operators.OperatorFactory;
import evaluator.operators.UnaryOperator;

public class UnaryOperation implements Expression {

    private final Expression operand;

    public UnaryOperation(Expression operand) {
        this.operand = operand;
    }
    
    @Override
    public Object evaluate() {
        return getOperator().evaluate(getOperand());
    }

    protected UnaryOperator getOperator() {
        return (UnaryOperator) new OperatorFactory().createOperator(this.getClass().getSimpleName(), this.getOperand());
    }

    protected Object getOperand() {
        return this.operand.evaluate();
    }

}
