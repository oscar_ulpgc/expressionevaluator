package evaluator;

public class Constant<T> implements Expression {

    private final T value;

    public Constant(T value) {
        this.value = value;
    }

    @Override
    public Object evaluate() {
        return value;
    }
}
