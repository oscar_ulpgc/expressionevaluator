package evaluator;

import evaluator.operators.OperatorFactory;
import evaluator.operators.BinaryOperator;
 
public abstract class BinaryOperation implements Expression {

    private final Expression left;
    private final Expression right;

    public BinaryOperation(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Object evaluate() {
        return getOperator().evaluate(getLeftOperand(), getRightOperand());
    }

    protected BinaryOperator getOperator() {
        return (BinaryOperator) new OperatorFactory().createOperator(this.getClass().getSimpleName(), this.getLeftOperand(), this.getRightOperand());
    }

    protected Object getLeftOperand() {
        return this.left.evaluate();
    }

    protected Object getRightOperand() {
        return this.right.evaluate();
    }
}
