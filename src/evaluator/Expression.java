package evaluator;

public interface Expression<T> {

    public T evaluate();
}
