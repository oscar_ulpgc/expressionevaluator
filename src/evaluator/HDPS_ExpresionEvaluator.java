package evaluator;

import java.util.Scanner;
import lexical.LexicalAnalyzer;
import parser.Parser;
import parser.ParserTreeBuildingStrategy;
import parser.ShuntingYardStrategy;
import parser.Token;

public class HDPS_ExpresionEvaluator {

    public static void main(String[] args) {
        System.out.println("Introduzca una expresión a evaluar: ");
        
        Scanner inputReader = new Scanner(System.in);
        
        String str_expression = inputReader.nextLine();
        
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] tokens = analyzer.analyze(str_expression);

        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);        

        Expression expression = parser.parse(tokens);
        
        System.out.println("El resultado de la evaluación es: " + expression.evaluate());
    }
}
