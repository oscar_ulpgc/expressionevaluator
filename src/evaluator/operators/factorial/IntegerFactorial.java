package evaluator.operators.factorial;

import evaluator.operators.minus.*;
import evaluator.operators.UnaryOperator;

public class IntegerFactorial extends UnaryOperator{

    @Override
    public Object evaluate(Object operand) {
        if ((int) operand == 0)
            return 1;
        else
            return (int) operand * (int) evaluate((int) operand - 1);
    }

}