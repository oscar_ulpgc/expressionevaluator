package evaluator.operators;

import evaluator.InvalidOperationException;

public class OperatorFactory {

    private String getSignature(String operation, Object[] operands) {
        return getPackage(operation) + getOperandsNames(operands) + operation;
    }

    private String getPackage(String operation) {
        return "evaluator.operators." + operation.toLowerCase() + ".";
    }

    public Operator createOperator(String operation, Object ... operands) {
        try {
            return (Operator) getClass(operation, operands).newInstance();
        } catch (InstantiationException | ClassNotFoundException | IllegalAccessException ex) {
            throw new InvalidOperationException();
        }
    }

    private Class<?> getClass(String operation, Object[] operands) throws ClassNotFoundException {
        return Class.forName(getSignature(operation, operands));
    }

    private String getOperandsNames(Object[] operands) {
        String names = "";
        for (Object operand : operands)
            names += operand.getClass().getSimpleName();
        return names;
    }
}
