package evaluator.operators;

public abstract class BinaryOperator extends Operator{

    public abstract Object evaluate(Object left, Object right);
}
