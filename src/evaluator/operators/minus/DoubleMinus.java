package evaluator.operators.minus;

import evaluator.operators.UnaryOperator;

public class DoubleMinus extends UnaryOperator {

    @Override
    public Object evaluate(Object operand) {
        return -1.0 * (Double) operand;
    }

}
