package evaluator.operators.minus;

import evaluator.operators.UnaryOperator;

public class IntegerMinus extends UnaryOperator{

    @Override
    public Object evaluate(Object operand) {
        return -1 * (Integer) operand;
    }
    

}
