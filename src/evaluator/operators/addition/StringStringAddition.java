
package evaluator.operators.addition;

import evaluator.operators.BinaryOperator;

public class StringStringAddition extends BinaryOperator {

    @Override
    public Object evaluate(Object left, Object right) {
        return (String) left + (String) right;
    }
    
}
