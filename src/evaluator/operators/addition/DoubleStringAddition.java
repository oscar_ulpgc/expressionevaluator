
package evaluator.operators.addition;

import evaluator.operators.BinaryOperator;

public class DoubleStringAddition extends BinaryOperator {

    @Override
    public Object evaluate(Object left, Object right) {
        return (Double) left + (String) right;
    }
    
}
