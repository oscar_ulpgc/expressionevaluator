
package evaluator.operators.addition;

import evaluator.operators.BinaryOperator;

public class IntegerStringAddition extends BinaryOperator {

    @Override
    public Object evaluate(Object left, Object right) {
        return (Integer) left + (String) right;
    }
    
}
