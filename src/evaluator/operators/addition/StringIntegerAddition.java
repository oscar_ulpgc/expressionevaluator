
package evaluator.operators.addition;

import evaluator.operators.BinaryOperator;

public class StringIntegerAddition extends BinaryOperator {

    @Override
    public Object evaluate(Object left, Object right) {
        return (String) left + (Integer) right;
    }
    
}
