package evaluator.operators;

public abstract class UnaryOperator extends Operator{
    
    public abstract Object evaluate(Object operand);
}
