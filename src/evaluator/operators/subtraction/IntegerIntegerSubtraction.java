
package evaluator.operators.subtraction;

import evaluator.operators.BinaryOperator;

public class IntegerIntegerSubtraction extends BinaryOperator {

    @Override
    public Object evaluate(Object left, Object right) {
        return (Integer) left - (Integer) right;
    }
    
}
