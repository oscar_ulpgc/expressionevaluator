
package evaluator.operators.modulo;

import evaluator.operators.BinaryOperator;

public class IntegerDoubleModulo extends BinaryOperator {

    @Override
    public Object evaluate(Object left, Object right) {
        return (int) left % (double) right;
    }
    
}
