
package evaluator.operators.modulo;

import evaluator.operators.BinaryOperator;

public class DoubleDoubleModulo extends BinaryOperator {

    @Override
    public Object evaluate(Object left, Object right) {
        return (double) left % (double) right;
    }
    
}
