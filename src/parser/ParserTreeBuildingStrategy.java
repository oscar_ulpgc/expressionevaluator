
package parser;

import evaluator.Expression;

public interface ParserTreeBuildingStrategy {
    public Expression analyze(Token ... tokens);
}
