
package parser;

import java.util.Objects;

public class Symbol extends Token {

    public static enum Type {
        UNARY, BINARY, OPEN_PARENTHESIS, CLOSE_PARENTHESIS
    }

    private final String token;
    private final Type type;
    private int precedence = 0;

    public Symbol(String token, Type type) {
        this.token = token;
        this.type = type;
    }

    public Symbol(String token, Type type, int precedence) {
        this(token, type);
        this.precedence = precedence;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Symbol other = (Symbol) obj;
        if (!Objects.equals(this.token, other.token)) {
            return false;
        }
        return true;
    }

    public boolean higherPrecedenceThan(Symbol symbol) {
        return this.precedence > symbol.precedence;
    }

    public Type getType() {
        return type;
    }
    
    public boolean isOperator() {
        return this.isUnary() || this.isBinary();
    }

    public boolean isUnary() {
        return this.type.equals(Type.UNARY);
    }

    public boolean isBinary() {
        return this.type.equals(Type.BINARY);
    }

    public boolean isOpenParenthesis() {
        return this.type.equals(Type.OPEN_PARENTHESIS);
    }

    public boolean isCloseParenthesis() {
        return this.type.equals(Type.CLOSE_PARENTHESIS);
    }

}
