
package parser;

public abstract class Token {
    
    public static <Type> Constant constant(Type value) {
        return new Constant(value);
    }
    
    public boolean isConstant() {
        return this instanceof Constant;
    }
    
    public boolean isSymbol() {
        return this instanceof Symbol;
    }
}
