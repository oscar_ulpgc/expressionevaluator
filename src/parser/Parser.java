
package parser;

import evaluator.Expression;

public class Parser {
    private final ParserTreeBuildingStrategy strategy;
    
    public Parser(ParserTreeBuildingStrategy strategy) {
        this.strategy = strategy;
    }
    
    public Expression parse(Token ... tokens) {        
        return strategy.analyze(tokens);
    }
}
