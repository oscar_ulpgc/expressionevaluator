package parser;

import java.util.HashMap;

public class OperatorSymbolColletion {
    
    public static final Symbol MINUS =             new Symbol("-", Symbol.Type.UNARY, 1);
    public static final Symbol ADDITION =          new Symbol("+", Symbol.Type.BINARY, 1);        
    public static final Symbol MULTIPLICATION =    new Symbol("*", Symbol.Type.BINARY, 2);
    public static final Symbol DIVISION =          new Symbol("/", Symbol.Type.BINARY, 2);
    public static final Symbol MODULO =            new Symbol("%", Symbol.Type.BINARY, 2);
    public static final Symbol FACTORIAL =         new Symbol("!", Symbol.Type.UNARY, 3);
    public static final Symbol OPEN_PARENTHESIS =  new Symbol("(", Symbol.Type.OPEN_PARENTHESIS);
    public static final Symbol CLOSE_PARENTHESIS = new Symbol(")", Symbol.Type.CLOSE_PARENTHESIS);
    
    private final HashMap<Character, Symbol> operatorDictionary;

    public OperatorSymbolColletion() {
        this.operatorDictionary = new HashMap<>();
        initDictionary();
    }
    
    public Symbol getToken(char symbol){
        return operatorDictionary.get(symbol);
    }
    
    public boolean isOperator(char symbol){
        return operatorDictionary.containsKey(symbol);
    }

    private void initDictionary() {
        operatorDictionary.put('+', ADDITION);
        operatorDictionary.put('-', MINUS);
        operatorDictionary.put('*', MULTIPLICATION);
        operatorDictionary.put('/', DIVISION);
        operatorDictionary.put('!', FACTORIAL);
        operatorDictionary.put('%', MODULO);
        operatorDictionary.put('(', OPEN_PARENTHESIS);
        operatorDictionary.put(')', CLOSE_PARENTHESIS);
    }
}
