package parser.expressions.unary;

import evaluator.Expression;
import evaluator.UnaryOperation;

public class Factorial extends UnaryOperation {

    public Factorial(Expression operand) {
        super(operand);
    }
    
}