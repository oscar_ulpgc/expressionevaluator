package parser.expressions.unary;

import evaluator.Expression;
import evaluator.UnaryOperation;

public class Minus extends UnaryOperation{

    public Minus(Expression operand) {
        super(operand);
    }
    
}
