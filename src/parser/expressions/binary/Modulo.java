
package parser.expressions.binary;

import evaluator.BinaryOperation;
import evaluator.Expression;

public class Modulo extends BinaryOperation {

    public Modulo(Expression left, Expression right) {
        super(left, right);
    }
    
}
