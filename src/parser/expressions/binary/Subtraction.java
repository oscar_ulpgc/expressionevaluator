
package parser.expressions.binary;

import evaluator.BinaryOperation;
import evaluator.Expression;

public class Subtraction extends BinaryOperation {

    public Subtraction(Expression left, Expression right) {
        super(left, right);
    }
    
}
