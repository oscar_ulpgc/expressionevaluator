
package parser.expressions.binary;

import evaluator.BinaryOperation;
import evaluator.Expression;

public class Division extends BinaryOperation {

    public Division(Expression left, Expression right) {
        super(left, right);
    }
    
}
