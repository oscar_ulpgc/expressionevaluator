
package parser.expressions.binary;

import evaluator.BinaryOperation;
import evaluator.Expression;

public class Multiplication extends BinaryOperation {

    public Multiplication(Expression left, Expression right) {
        super(left, right);
    }
    
}
