package parser.expressions.binary;

import evaluator.BinaryOperation;
import evaluator.Expression;

public class Addition extends BinaryOperation {

    public Addition(Expression left, Expression right) {
        super(left, right);
    }
}
