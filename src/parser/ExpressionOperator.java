
package parser;

import evaluator.Expression;
import java.lang.reflect.InvocationTargetException;
import java.util.Stack;

public interface ExpressionOperator {

    public Expression getInstance(String className, Stack<Expression> expressions) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException;
    
}
