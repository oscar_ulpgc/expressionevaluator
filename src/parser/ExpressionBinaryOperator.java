
package parser;

import evaluator.Expression;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Stack;

public class ExpressionBinaryOperator implements ExpressionOperator {

    @Override
    public Expression getInstance(String className, Stack<Expression> expressions) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        Constructor<?> expressionConstructor = Class.forName(className).getConstructor(new Class[] {Expression.class, Expression.class});
        Expression right = expressions.pop();
        Expression left = expressions.pop();
        return (Expression) expressionConstructor.newInstance(left, right);
    }
    
}
