package parser;

import evaluator.Constant;
import evaluator.Expression;
import java.util.Stack;
 
public class ShuntingYardStrategy implements ParserTreeBuildingStrategy {

    private final Stack<Symbol> symbols;
    private final Stack<Expression> expressions;
    
    public ShuntingYardStrategy() {
        this.symbols = new Stack<>();
        this.expressions = new Stack<>();
    }

    @Override
    public Expression analyze(Token[] tokens) {
        for (Token token : tokens) analyze(token);
        while (! symbols.isEmpty()) operate(symbols.pop());

        return expressions.pop();
    }

    private void analyze(Token token) {
        if (token instanceof parser.Symbol)
            analyze((parser.Symbol) token);
        else if (token instanceof parser.Constant)
            analyze((parser.Constant) token);
    }

    private void analyze(parser.Constant token) {
        expressions.push(new Constant(token.getValue()));
    }
    
    private void analyze(parser.Symbol token) {
        if (token.isCloseParenthesis())
            operateParenthesis();
        else
            stackSymbol(token);
    }

    private void operateParenthesis() {
        while (! symbols.lastElement().isOpenParenthesis())
            operate(symbols.pop());        
        symbols.pop();
    }
    
    private Symbol stackSymbol(parser.Symbol token) {
        if (! isStackAllowed(token)) operate(symbols.pop());
        
        if (symbolIsMinus(token) && !expressions.isEmpty())
            symbols.push(OperatorSymbolColletion.ADDITION);
        
        return symbols.push(token);
    }

    private static boolean symbolIsMinus(parser.Symbol token) {
        return ((parser.Symbol) token).equals(OperatorSymbolColletion.MINUS);
    }

    private void operate(parser.Symbol s) {
        expressions.push((new ExpressionFactory()).createExpression(s, expressions));
    }

    private boolean isStackAllowed(parser.Symbol token) {
        return (symbols.isEmpty()
                || symbols.lastElement().isOpenParenthesis() || token.isOpenParenthesis()
                || token.higherPrecedenceThan(symbols.lastElement())) &&
                ! token.isCloseParenthesis();
    }

}
