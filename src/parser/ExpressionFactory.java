package parser;

import evaluator.Expression;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Stack;
import javax.management.RuntimeErrorException;
import parser.expressions.binary.*;
import parser.expressions.unary.*;

public class ExpressionFactory {       
    
    HashMap<Symbol, String> dictionary;

    public ExpressionFactory() {
        dictionary = new HashMap<>();
        dictionary.put(OperatorSymbolColletion.MINUS, Minus.class.getName());
        dictionary.put(OperatorSymbolColletion.ADDITION, Addition.class.getName());        
        dictionary.put(OperatorSymbolColletion.MULTIPLICATION, Multiplication.class.getName());
        dictionary.put(OperatorSymbolColletion.DIVISION, Division.class.getName());
        dictionary.put(OperatorSymbolColletion.MODULO, Modulo.class.getName());
        dictionary.put(OperatorSymbolColletion.FACTORIAL, Factorial.class.getName());
    }    
    
    public Expression createExpression(parser.Symbol symbol, Stack<Expression> expressions) throws RuntimeException {
        try {
            if (!symbol.isOperator()) throw new RuntimeException("Symbol is not an operator");
            ExpressionOperator eo = (ExpressionOperator) Class.forName(getSignature(symbol)).newInstance();
            return eo.getInstance(dictionary.get(symbol), expressions);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | RuntimeException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private String getSignature(Symbol symbol) {
        String type = symbol.getType().toString();
        return "parser.Expression" + type.substring(0, 1).toUpperCase() + type.substring(1).toLowerCase() + "Operator";
    }
    
}
