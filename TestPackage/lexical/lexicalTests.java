package lexical;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import parser.Constant;
import parser.OperatorSymbolColletion;
import parser.Token;
import static parser.Token.*;

public class lexicalTests {
    
    @Test
    public void constantTokenTest(){
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] expected = new Token[] {
            new Constant(2)
        };
        assertEquals(expected, analyzer.analyze("2"));
    }
    
    @Test
    public void twoConstantAndOneSymbolTokenTest(){
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] expected = {
            constant(2),
            OperatorSymbolColletion.ADDITION,
            constant(3)
        };
        assertEquals(expected, analyzer.analyze("2+3"));
    }
    
    @Test
    public void twoConstantOneDoubleAndOneSymbolTokenTest(){
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] expected = {
            constant(2.3),
            OperatorSymbolColletion.ADDITION,
            constant(3)
        };
        assertEquals(expected, analyzer.analyze("2.3+3"));
    }
    
    @Test
    public void multipleSymbolTest(){
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] expected = {
            constant(2.3),
            OperatorSymbolColletion.ADDITION,
            constant(3),
            OperatorSymbolColletion.MINUS,
            constant(5.2),
            OperatorSymbolColletion.MULTIPLICATION,
            constant(2)
        };
        assertEquals(expected, analyzer.analyze("2.3+3-5.2*2"));
    }
    
    @Test
    public void multipleSymbolWithParenthesisTest(){
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] expected = {
            constant(2.3),
            OperatorSymbolColletion.ADDITION,
            OperatorSymbolColletion.OPEN_PARENTHESIS,
            constant(3),
            OperatorSymbolColletion.MINUS,
            constant(5.2),
            OperatorSymbolColletion.CLOSE_PARENTHESIS,
            OperatorSymbolColletion.MULTIPLICATION,
            constant(2)
        };
        assertEquals(expected, analyzer.analyze("2.3+(3-5.2)*2"));
    }
    
    @Test
    public void withSpacesTest(){
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] expected = {
            constant(2.3),
            OperatorSymbolColletion.ADDITION,
            OperatorSymbolColletion.OPEN_PARENTHESIS,
            constant(3),
            OperatorSymbolColletion.MINUS,
            constant(5.2),
            OperatorSymbolColletion.CLOSE_PARENTHESIS,
            OperatorSymbolColletion.MULTIPLICATION,
            constant(2)
        };
        assertEquals(expected, analyzer.analyze("2.3 + (3-5.2) * 2"));
    }
    
    @Test
    public void withEverythingTest(){
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] expected = {
            constant(2.3),
            OperatorSymbolColletion.ADDITION,
            constant("abcd"),
            OperatorSymbolColletion.OPEN_PARENTHESIS,
            constant(3),
            OperatorSymbolColletion.MINUS,
            constant(5.2),
            OperatorSymbolColletion.CLOSE_PARENTHESIS,
            OperatorSymbolColletion.MULTIPLICATION,
            constant("2aa")
        };
        assertEquals(expected, analyzer.analyze("2.3 + abcd(3-5.2) * 2aa"));
    }
}