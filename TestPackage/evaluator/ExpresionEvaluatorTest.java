package evaluator;

import parser.expressions.binary.Modulo;
import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.Test;
import parser.expressions.binary.*;
import parser.expressions.unary.*;

public class ExpresionEvaluatorTest {

    private final double delta = 0.001;
    
    @Test
    public void testConstantExpressions() {
        assertEquals(1, (int) new Constant(1).evaluate());
        assertEquals(8.0, (double) new Constant(8.0).evaluate(), delta);
    }    
    
    @Test
    public void testAdditionTypes() {
        assertEquals(2, (int) new Addition(new Constant(1), new Constant(1)).evaluate());
        assertEquals(2.5, (double) new Addition(new Constant(1), new Constant(1.5)).evaluate(), delta);
        assertEquals(2.5, (double) new Addition(new Constant(1.5), new Constant(1)).evaluate(), delta);
        assertEquals(5, (double) new Addition(new Constant(2.5), new Constant(2.5)).evaluate(), delta);        
        assertEquals("a1", (String) new Addition(new Constant("a"), new Constant(1)).evaluate());
        assertEquals("1a", (String) new Addition(new Constant(1), new Constant("a")).evaluate());
        assertEquals("a1.5", (String) new Addition(new Constant("a"), new Constant(1.5)).evaluate());
        assertEquals("1.5a", (String) new Addition(new Constant(1.5), new Constant("a")).evaluate());
        assertEquals("AaBb", (String) new Addition(new Constant("Aa"), new Constant("Bb")).evaluate());
    }
    
    @Test
    public void testSubtractionTypes() {
        assertEquals(3, (int) new Subtraction(new Constant(5), new Constant(2)).evaluate());
        assertEquals(3.2, (double) new Subtraction(new Constant(4), new Constant(0.8)).evaluate(), delta);
        assertEquals(5.1, (double) new Subtraction(new Constant(7.1), new Constant(2)).evaluate(), delta);
        assertEquals(2, (double) new Subtraction(new Constant(4.5), new Constant(2.5)).evaluate(), delta);
    }
    
    @Test
    public void testMultiplicationTypes() {
        assertEquals(10, (int) new Multiplication(new Constant(5), new Constant(2)).evaluate());
        assertEquals(2.8, (double) new Multiplication(new Constant(4), new Constant(0.7)).evaluate(), delta);
        assertEquals(14.2, (double) new Multiplication(new Constant(7.1), new Constant(2)).evaluate(), delta);
        assertEquals(11.25, (double) new Multiplication(new Constant(4.5), new Constant(2.5)).evaluate(), delta);
    }
    
    @Test
    public void testDivisionTypes() {
        assertEquals(2, (int) new Division(new Constant(5), new Constant(2)).evaluate(), delta);
        assertEquals(5.714, (double) new Division(new Constant(4), new Constant(0.7)).evaluate(), delta);
        assertEquals(3.55, (double) new Division(new Constant(7.1), new Constant(2)).evaluate(), delta);
        assertEquals(1.8, (double) new Division(new Constant(4.5), new Constant(2.5)).evaluate(), delta);
    }
    
    @Test
    public void testInvalidAdd() {
        try {
            assertEquals(3.7, (double) new Addition(new Constant(1.2), new Constant(new ArrayList<>())).evaluate(), delta);
            fail("InvalidOperationException not thrown");
        } catch (InvalidOperationException ex) {
            assertTrue(true);
        } catch (Exception ex) {
            fail("InvalidOperationException not thrown");
        }
    }
    
    @Test
    public void testMinusType(){
        assertEquals(-5, (int) new Minus(new Constant(5)).evaluate());
        assertEquals(-5.0, (Double) new Minus(new Constant(5.0)).evaluate(), delta);
    }
    
    @Test
    public void complexExpressionTest(){
        assertEquals(-12.5, (Double) 
                new Multiplication(new Addition(new Constant(1), new Constant(1.5)), new Minus(new Constant(5))).evaluate(), delta);
    }
    
    @Test
    public void testFactorialTypes() {
        assertEquals(120, (int) new Factorial(new Constant(5)).evaluate());
    }
    
    @Test
    public void testModuloTypes() {
        assertEquals(1, (int) new Modulo(new Constant(4), new Constant(3)).evaluate());
        assertEquals(1.1, (double) new Modulo(new Constant(4.1), new Constant(3)).evaluate(), delta);
        assertEquals(0.9, (double) new Modulo(new Constant(4), new Constant(3.1)).evaluate(), delta);
        assertEquals(0.8, (double) new Modulo(new Constant(4.1), new Constant(3.3)).evaluate(), delta);
    }
}