package parser;

import parser.expressions.binary.*;
import evaluator.Constant;
import parser.expressions.unary.Minus;
import evaluator.Expression;
import lexical.LexicalAnalyzer;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static parser.Token.*;
import static org.junit.Assert.*;

public class ParserTest {

    private final double delta = 0.001;
    
    @Test
    public void testParserTreeBuildingStrategyIntegration() {
        ParserTreeBuildingStrategy strategy = mock(ParserTreeBuildingStrategy.class);
        Parser parser = new Parser(strategy);                
        
        Expression expression = parser.parse(new Token[] {});
        verify(strategy).analyze(new Token[] {});
    }
    
    @Test
    public void testSimpleExpression() {
        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);
        
        Token[] tokens = {
            constant(1),
            OperatorSymbolColletion.ADDITION,
            constant(2)
        };
        
        Expression expression = parser.parse(tokens);
        assertEquals(new Addition(new Constant(1), new Constant(2)).evaluate(), expression.evaluate());
    }
    
    @Test
    public void testMinusExpression() {
        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);
        
        Token[] tokens = {            
            OperatorSymbolColletion.MINUS,
            constant(2)
        };
        
        Expression expression = parser.parse(tokens);
        assertEquals(new Minus(new Constant(2)).evaluate(), expression.evaluate());
    }
    
    @Test
    public void testMinusMinusExpression() {
        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);
        
        Token[] tokens = {            
            constant(5),
            OperatorSymbolColletion.MINUS,
            constant(3),
            OperatorSymbolColletion.MINUS,
            constant(1)
        };
        
        Expression expression = parser.parse(tokens);
        assertEquals(
                new Addition(
                        new Addition(new Constant(5), new Minus(new Constant(3))),
                        new Minus(new Constant(1)))
                .evaluate(), expression.evaluate());
    }
    
    @Test
    public void testOperatorsWithDifferentPrecedence() {
        
        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);
        
        Token[] tokens = {            
            constant(2),
            OperatorSymbolColletion.MULTIPLICATION,
            constant(3),
            OperatorSymbolColletion.ADDITION,
            constant(5)
        };
        
        Expression expression = parser.parse(tokens);
        assertEquals(
            new Addition(
                new Multiplication(new Constant(2), new Constant(3)),
                new Constant(5)
            ).evaluate(),
            expression.evaluate());
    }
    
    @Test
    public void testOperationWithParenthesis() {
        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);
        
        Token[] tokens = {            
            constant(2),
            OperatorSymbolColletion.MULTIPLICATION,
            OperatorSymbolColletion.OPEN_PARENTHESIS,
            constant(3),
            OperatorSymbolColletion.ADDITION,
            constant(5),
            OperatorSymbolColletion.CLOSE_PARENTHESIS
        };
        
        Expression expression = parser.parse(tokens);
        assertEquals(
            new Multiplication(
                new Constant(2),
                new Addition(new Constant(3), new Constant(5))
            ).evaluate(),
            expression.evaluate());
    }
    
    @Test
    public void testOperationWithMultipleParenthesis() {
        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);
        
        Token[] tokens = {            
            constant(2),
            OperatorSymbolColletion.MULTIPLICATION,
            OperatorSymbolColletion.OPEN_PARENTHESIS,
            constant(3),
            OperatorSymbolColletion.ADDITION,
            constant(5),
            OperatorSymbolColletion.CLOSE_PARENTHESIS,
            OperatorSymbolColletion.MULTIPLICATION,
            OperatorSymbolColletion.OPEN_PARENTHESIS,
            constant(2),
            OperatorSymbolColletion.ADDITION,
            constant(4),
            OperatorSymbolColletion.CLOSE_PARENTHESIS
        };        
        
        Expression expression = parser.parse(tokens);
        assertEquals(
            new Multiplication(
                new Constant(2),
                new Multiplication(
                    new Addition(new Constant(3), new Constant(5)),
                    new Addition(new Constant(2), new Constant(4))
                )
            ).evaluate(),
            expression.evaluate());
    }
    
    @Test
    public void testOperationUsingLexicalAnalyzer() {
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] tokens = analyzer.analyze("2.3+(3-5.2)*2");
        
        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);        
        
        Expression expression = parser.parse(tokens);
        
        assertEquals(-2.1, (double) expression.evaluate(), delta);
    }
    
    @Test
    public void testOperationWithNestedParenthesis() {
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] tokens = analyzer.analyze("2.3+(5*(3-5.2)-3)*2");
        
        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);        
        
        Expression expression = parser.parse(tokens);
        
        assertEquals(-25.7, (double) expression.evaluate(), delta);
    }
    
    @Test
    public void testSimpleFactorial() {
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] tokens = analyzer.analyze("5!");
        
        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);        
        
        Expression expression = parser.parse(tokens);
        
        assertEquals(120, (int) expression.evaluate());
    }
    
    @Test
    public void testComplexFactorial() {
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] tokens = analyzer.analyze("5!*2+1");

        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);        

        Expression expression = parser.parse(tokens);

        assertEquals(241, (int) expression.evaluate());
    }
    
    @Test
    public void testModulo() {
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] tokens = analyzer.analyze("4%3");

        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);        

        Expression expression = parser.parse(tokens);

        assertEquals(1, (int) expression.evaluate());
    }
    
    @Test
    public void testComplexModulo() {
        LexicalAnalyzer analyzer = new LexicalAnalyzer();
        Token[] tokens = analyzer.analyze("4.1 % 3.3 + 4");

        ParserTreeBuildingStrategy strategy = new ShuntingYardStrategy();
        Parser parser = new Parser(strategy);        

        Expression expression = parser.parse(tokens);

        assertEquals(4.8, (double) expression.evaluate(), delta);
    }
}
